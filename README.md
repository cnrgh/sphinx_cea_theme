# Sphinx CEA Theme

CEA documentation theme for Sphinx.

## Getting Started

### Prerequisites

You need a project with `sphinx` and `sphinx_rtd_theme` installed. \
The following steps will show you how to install them into your project.

**I. Python virtual environment**

Go inside the top-level directory of your project. \
If you don't have a python virtual environment for you project, create one and activate it : 

```bash
python3 -m venv venv
source venv/bin/activate
```

**II. Sphinx and Sphinx Read the Docs Sphinx Theme installation**

Now, install `sphinx` and `sphinx_rtd_theme` :
```bash
pip3 install sphinx
pip3 install sphinx_rtd_theme
```

Always at the top-level directory of your project, create a `docs` directory and go inside :
```bash
mkdir docs
cd docs/
```

Execute the sphinx quickstart utility to set up a sphinx default configuration.\
When the utility asks `> Separate source and build directories (y/n) [n]:`, answer **y**.
```bash
sphinx-quickstart
```

### Installing

Download the `sphinx_cea_theme.wheel` file from the download button at the top right of the screen.

In the directory where the file is saved : 

```bash
pip3 install name_of_the_downloaded_file.whl
```

Then, assuming the **prerequisites** are respected, go into `docs/source/` and edit the following line in `conf.py` to apply the CEA theme :

```python
html_theme = 'sphinx_cea_theme'
```

### Generating documentation

Edit your documentation by updating the file `index.rst` in `docs/source/`.

Generate your documentation by running the following command in `docs/` :
```bash
make html
```

Open the file `index.html` located in `docs/build/html/` with any browser to view the documentation.

## For developers

You need to respect the **prerequisites**. \
Clone this repository (`sphinx_cea_theme`), go inside and install the CEA theme with its dependencies :
```bash
cd sphinx_cea_theme/
pip3 install -e .
python3 setup.py bdist_wheel
```

> ##### Tips
> You can install this theme for only one project with by using a venv.
> ```bash
> python3 -m venv venv #create a venv
> source venv/bin/activate # activate the venv
> pip3 install -e .
> python3 setup.py bdist_wheel
> deactivate # desactivate the venv for the next steps
> ```

Go inside the top-level directory of any project and install the CEA theme with the following commands (`my_project_directory` and `sphinx_cea_theme` are at the same level): 

```
.
+-- sphinx_cea_theme/
+-- my_project_directory/
```

```bash
cd my_project_directory/
source venv/bin/activate #activate the venv of my_project_directory
pip3 install ../sphinx_cea_theme/dist/sphinx_cea_theme-1.0.0-py3-none-any.whl
```

Go into `docs/source/` and edit the following line in `conf.py` to apply the CEA theme :

```python
html_theme = 'sphinx_cea_theme'
```

## Based on

* [Sphinx](http://www.sphinx-doc.org/en/master/) - Free software for generating documentation.
* [Sphinx Read the Docs theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/) - Theme for Sphinx, on which the Sphinx CEA theme is based.


