from setuptools import setup


setup(
    name='sphinx_cea_theme',
    version='1.0.0',
    url='https://gitlab.com/cnrgh/sphinx_cea_theme',
    license='CeCCIL',
    author='LBI team',
    author_email='bioinfo@cnrgh.fr',
    description='CEA Docs theme for Sphinx',
    zip_safe=False,
    packages=['sphinx_cea_theme'],
    package_data={'sphinx_cea_theme': [
        'theme.conf',
        '*.html',
        'static/css/*.css',
        'static/css/fonts/*.*'
        'static/js/*.js',
        'static/img/*'
    ]},
    include_package_data=True,
    # See http://www.sphinx-doc.org/en/stable/theming.html#distribute-your-theme-as-a-python-package
    entry_points={
        'sphinx.html_themes': [
            'sphinx_cea_theme = sphinx_cea_theme',
        ]
    },
    install_requires=[
       'sphinx >= 2.3.0',
       'sphinx-rtd-theme >= 0.4.3'
    ],
    tests_require=[
        'pytest'
    ],
    extras_require={
        'dev': [
            'transifex-client',
            'sphinxcontrib-httpdomain',
        ],
    },
    classifiers=[
        'Framework :: Sphinx',
        'Framework :: Sphinx :: Theme',
        'Development Status :: 5 - Production/Stable',
        'License :: OSI Approved :: CEA CNRS Inria Logiciel Libre License, version 2.1 (CeCILL-2.1)',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Operating System :: OS Independent',
        'Topic :: Documentation',
        'Topic :: Software Development :: Documentation',
    ],
)

